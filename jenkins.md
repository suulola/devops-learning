
## For freestyle project

```js
- Click on <strong>New Item</strong> to create a new project
- Input the Project name
- Select Freestyle project
- Select OK to proceed 
- On the Configuration page
* Under the source control management, Select the source as git and  input the git repository url
* Under the Build Environment, 
for a maven project, select "Invoke top-level Maven targets" and specify the commands e.g "verify" that is specific to maven that is to be executed.
for dot net project, select "Execute Windows batch command" or install MS Build, Nuget and NUnit package tool installed via the Plugin
```

## For Pipeline project

```js
- Click on <strong>New Item</strong> to create a new project
- Input the Project name
- Select Pipeline Project
- Select OK to proceed 
- On the Configuration page
* Enter the Pipeline Script. You can click on the "Pipeline syntax" link for a GUI to generate the syntax. 
```

Pipeline Syntax Example

```js
node('main') {
  stage('checkout code') {
    git 'https://github.com_repo.git'
  }
  stage('Restore Nuget') {
    bat 'C://path_on_drive.sln'
  }
  stage('Build') {
     bat '...command'
  }
  stage('Test') {
     bat '...command'
  }
}
```

How to Install Plugin

* Go to "Manage Jenkins"
* Go to "Manage Plugins"
* Go to the "Available" tab
* Search for the "Plugin" e.g msbuild
* Check and select the plugin and Click on "Install without restart"
* Logout and login back to see the plugin active
* To configure the Plugin, go to "Manage Jenkins"
* Go to "Global Tool Configuration"
* Make the necessary path updates/configuration updates and "Save" 


-----

# Install Jenkins

```js
sudo apt update -y
sudo apt install openjdk-11-jre -y // Install Java
curl -fsSL https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo tee \
  /usr/share/keyrings/jenkins-keyring.asc > /dev/null
echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] \
  https://pkg.jenkins.io/debian-stable binary/ | sudo tee \
  /etc/apt/sources.list.d/jenkins.list > /dev/null
sudo apt-get update
sudo apt-get install jenkins


// https://www.jenkins.io/doc/book/installing/linux/
```

FOR REDHAT OR AMAZON LINUX

```
sudo yum update -y
sudo yum install epel-release -y
sudo yum install java -y
sudo wget -O /etc/yum.repos.d/jenkins.repo https://pkg.jenkins.io/redhat-stable/jenkins.repo --no-check-certificate
sudo rpm --import http://pkg.jenkins.io/redhat-stable/jenkins.io.key
sudo yum install jenkins -y
sudo systemctl start jenkins
sudo systemctl status jenkins
sudo cat /var/lib/jenkins/secrets/initialAdminPassword
```

To start jenkins

```js
sudo systemctl start jenkins
systemctl status jenkins // to confirm it is active
sudo ufw allow 8080 // open up firewall to allow us interact with jenkins from the UI
sudo ufw status // to confirm it is now inactive
sudo cat /var/lib/jenkins/secrets/initialAdminPassword // to print out initial password
```