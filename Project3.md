# MERN STACK IMPLEMENTATION

* M- MongoDB database
* E- Express
* R- ReactJS
* N- Node

1. Create an EC2 instance on AWS for the project and ensure inbound rule allows 

- SSH connection 
- HTTP connection
-  Custom TCP connection on both port 3000(front-end) and port 5000(backend) and confirm the application can be accessed using the public IP
![Project 3 Inbound Rule settings](./images/three/inbound.png)


2.Update OS and install Node

```
sudo apt update
sudo apt upgrade -y // deletes the previous version. -y flag implies YES to all permission prompts

sudo apt install nodejs -y
--------------OR------------
sudo apt install snapd // if it is not already installed
sudo snap install node --classic
```
<br/>
I'll use the first approach of using <strong>snap</strong>

![Project 3 Inbound Rule settings](./images/three/snapd.png)


3. Set up project locally and push to Github

4. Clone project within a directory

```
git clone https://gitlab.com/suulola/todo-app
cd todo-app
npm install
```
![Project 3 Inbound Rule settings](./images/three/clone_install_start.png)

5. Start the application using 

```
npm run dev
```
You should be able to access the application on a browser using the public_ip_address:port_3000 for the Front end application while the backend application can be accessed on ip_address:5000/api/v1/todos

You can see the IP address on the terminal by running
```
curl https://checkip.amazonaws.com/
```
![checkip](./images/three/checkip.png)


<br>
<br>
<strong>Examples</strong>
<br />
<i>Front-end: http://54.89.157.174:3000<i>
<br>
<i>Back-end: http://54.89.157.174:5000<i>



### BACKEND SETUP

1. After setting up NodeJS, run the following commands 
```
npm install -g typescript-express-generator // to install the express project generator which uses nodemon by default
npm install -g express-mongoose-generator
ts-express --no-view Todo // to generate the project called Todo with no views
cd Todo
code . // to view the codebase using vscode
npm install
npm start
```
NB: Change the port number to 5000 in the bin/www.ts file before running <b>npm start</b>

2. Install moogoose for MongoDB database
```
npm install mongoose
```

3. Create a mongodb account at https://www.mongodb.com/cloud/atlas/register to create account that will be used for 
- creating a database
- configuring the database access - means of authentication
- configuring the network access - the IP that can access it

![mongo atlas](./images/three/mongo_atlas.png)

4. Generate Todo model, controller and route files and update the import in src/app.ts by import TodeRoute in app.ts and specify the /todo route.

```
cd src
mongoose-gen --ts
```

5. Configure/Add the database to the project and all should be set to test.
![API](./images/three/api.png)


6. Push to Git repository
![useful git commands](./images/three/git.png)


### Front end
1. Initialize the app in the root directory using the create-react command
```
npx create-react-app client --template typescript
```

2. Install concurrent package in the root directory https://www.npmjs.com/package/concurrently

```
npm install concurrently --save-dev
```

3. Update the root package.json command to start both the backend and frontend
```js
 "server": "nodemon --exec ts-node ./bin/www.ts",
 "dev": "concurrently \"npm run server\" \"cd client && npm start\""
```
![package json script](./images/three/script.png)


4. Configure Front end proxy to http://localhost:5000 in 


5. Start the application using npm run dev and navigate to the client folder to make changes to the react app

```
npm run dev
```

6. Make changes to the application to perform CRUD operation
![front end code](./images/three/code.png)


7. Test and Push to Git repository

![front end ui](./images/three/front.png)
