# About software development life cycle (SDLC)

- Software Development Life Cycle (SDLC) is a process used by the software industry to design, develop and test high quality softwares.
- The SDLC aims to produce a high-quality software that meets or exceeds customer expectations, reaches completion within times and cost estimates.

It involves the following cycle.
![Stages of the development Cycle](https://phoenixnap.com/blog/wp-content/uploads/2021/08/Phases-of-Software-Development-Life-Cycle.png)

## Some important and popular SDLC models followed in the industry

- Waterfall Model
- V-Shaped Model - Verification and Validation Model
- Prototype Model - prototype is developed prior to the actual software.
- Agile Model - focuses more on flexibility while developing a product rather than on the requirement.

----
chmod - change mode - change the operation the user can perform
chown - change owner - changes the owner of the owner

----

- TCP - Transmission Control Protocol 
- UPD - User Datagram Protocol
