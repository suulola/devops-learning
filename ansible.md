To change the name/description shown in the terminal 
```
sudo vi /etc/hostname
sudo vi /etc/hosts
shutdown now -r
```

Install ansible

```js
sudo yum install ansible
ansible --version
```

## Enabling SSH on the VM

If you need SSH enabled on the system, follow the below steps:

* Ensure the /etc/apt/sources.list file has been updated as per above

* Run the command: apt-get update 

* Run the command: apt-get install openssh-server

* Run the command: service sshd start


--------------------
 ```
 cat inventory.txt
 target1 ansible_host=172.31.90.203 ansible_ssh_pass=password
 ansible target1 -m ping -i inventory.txt
```



```
sql_db1 ansible_host=sql01.xyz.com ansible_connection=ssh ansible_user=root ansible_ssh_pass=Lin$Pass
sql_db2 ansible_host=sql02.xyz.com ansible_connection=ssh ansible_user=root ansible_ssh_pass=Lin$Pass

web_node1 ansible_host=web01.xyz.com ansible_connection=winrm ansible_user=administrator ansible_password=Win$Pass
web_node2 ansible_host=web02.xyz.com ansible_connection=winrm ansible_user=administrator ansible_password=Win$Pass
web_node3 ansible_host=web03.xyz.com ansible_connection=winrm ansible_user=administrator ansible_password=Win$Pass

[db_nodes]
sql_db1
sql_db2

[web_nodes]
web_node1
web_node2
web_node3

[boston_nodes]
sql_db1
web_node1

[dallas_nodes]
sql_db2
web_node2
web_node3

[us_nodes:children]
boston_nodes
dallas_nodes
```