#! /bin/bash
USERFILE=$(cat names.csv)
PASSWORD=password

USERID=$(id -u)

function createUser () {
  echo "$1 isnt a user yet"

# Create the user
  useradd -m -d /home/$1 -s /bin/bash -g developers $1
  echo "$1 is now a user" 

  # create the ssh folder in the user home file
  su - -c "mkdir ~/.ssh" $1
  echo "SSH folder created for $1"

  # Set user permission for SSH directory
  su - -c "chmod 700 ~/.ssh" $1
  echo "SSH permission set"

  # create an authorized key file
  su - -c "touch ~/.ssh/authorized_keys" $1
  echo "Authorized key created"

  # Set permission for the authorized key file
  su - -c "chmod 600 ~/.ssh/authorized_keys" $1
  echo "Authorized keys permission set"

  cp -R "/home/ubuntu/Shell/id_rsa.pub" "/home/$1/.ssh/authorized_keys"
  echo "Public key copied to user's account from server"

  # Generate a password
  sudo echo -e "$PASSWORD\n$PASSWORD" | sudo passwd "$1"
  sudo passwd -x 5 $1
}

function performUserCheck () {
  for USER in $USERFILE
    do
    if id "$USER" &>/dev/null
    then
      echo "$USER already exists"
    else
      createUser $USER
      fi
    done
}

if [ $USERID -eq 0 ]; then
performUserCheck
else
echo "Not a super user"
fi
