## Vim File Control
To open a file with vim in command window.
```
vim names.txt
vi names.txt about.html
```
To go to the next file or the previous line
 ```
ESC :next 
ESC :previous
```


ESC :tabe hello.java Open a new file while vim is already opened with one file, if the file doesn’t exist, it will create it.
ESC :tabn or ESC :tabp Go to the next file or previous file accordingly.
ESC :wqa To save works in all tabs and quit vim.
ESC :bd To close the file without quitting the vim.
ESC :w backup.txt Save the current file to a new file with the file name “backup.txt”.
ESC :w Save changes.
ESC :wq Save the changes and exit.
ESC :q! Quit without save.

## Vim Edit mode or insert mode

ESC i Enter insert mode and start editing.
3. Vim Undo and Redo

ESC u Undo the changes.
ESC Ctrl-r Redo the changes.
4. Vim Line Control

ESC 0 or ESC Home Move the cursor to the beginning of the current line.
ESC $ or ESC End Move the cursor to the end of the current line.
ESC :42 Go to line 42.
ESC gg Go to the beginning of file.
ESC G Go to the end of file.
ESC ( Jump backward one sentence.
ESC ) Jump forward one sentence.
ESC dd Delete the current line.
5. Vim Cursor Control

h Move Left
l Move Right
j Move Down
k Move Up
6. Vim Screen Control

ESC zt Move the cursor to the top of the screen.
ESC zz Move the cursor to the middle of the screen.
ESC zb Move the cursor to the bottom of the screen.
ESC Ctrl-f Scroll down one screen page.
ESC Ctrl-b Scroll up one screen page.
7. Vim Search and Replace

ESC /hello and enter Search for the word “hello”. To find the next occurance, press n, the previous occurance, press N.
ESC :s/foo/bar/g Search and replace “foo” with “bar” in the current line.
ESC :%s/foo/bar/gc Search and ask first and then you choose if you want to replace “foo” with “bar”.
ESC :%s/foo/bar/g Search and replaces all “foo” with bar in the file.
ESC :%s/[0-9]/a/g Find all digits and replaces it with “a”.
8. Vim Copy and Paste

ESC v and move the cursor to select the text you want to cut or copy.
Press d to cut.
Press y to copy.
Move to where you want to paste it and Press p to paste it.
9. Vim Help

ESC :help Open the help page.
ESC :q To exit the help.