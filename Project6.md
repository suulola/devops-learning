# WordPress

## 1. Spin Up 2 AWS EC2 instances using Redhat Operating System

![AWS instance](./images/six/redhat.png)

One will house the wordpress(Web Server) while the second will house the mysql database(DB Server)

## 2. Create Volumes

Under the Elastic Block Store, Click on <strong>Volumes</strong> to create volumes
<br />

![creating volume](./images/six/create_volume.png)

Ensure the <strong>Availability Zone</strong> is same with the Availability Zone for the EC2 instance and the Size 10GB

## 3. Attach the 3 Volumes to the Web Server EC2 Instance

![list volume](./images/six/volume_list.png)

Click on the Action button with a volume being checked, to attach the volume. Ensure the volumes are attached to the Data Server EC2 instance.

![attach volume](./images/six/attach_volume.png)


## 4. SSH into the Web Server EC2 instance to confirm the volumes attached are reflected.

```
ssh -i "suu.pem" ec2-user@ec2-35-171-19-223.compute-1.amazonaws.com
lsblk // display details about block devices connected to the instance
df -h // disk filesystem in human readable form. At the moment, the newly created volumes won't reflect in the disk filesystem
```

![confirm mounted volume](./images/six/lsblk.png)


## 5. Create a single partition on each of the 3 disks using gdisk

```
sudo gdisk /dev/xvdf
```
 
* Press <strong>n</strong> when the prompt shows to create a new partition

* Choose default for all the options to create 1 partition and use total disk space for the partitioning

* Change the Type of the partition from Linux Filesystem - 8300(which is the default) to Linux LVM - 8e00 (Logical Volume Management)

* Press <strong>p</strong> to preview the selection and if it's all good
* Press <strong>w</strong> to write and and confirm to complete the operation

Repeat the same process for the two other volumes - xvdg and xvdh.

![create partition](./images/six/partition.png)


## 6. Confirm the creation of the partitions

```
lsblk 
```

## 7. Create Physical Volumes, Volume Groups and Logical Volume from the partitions created.

* Install LVM2
```
sudo yum install lvm2
```
![install lvm2](./images/six/lvm2.png)


LVM is a method of allocating hard drive(s) space into logical volumes that can be easily resized instead of partitions

![about lvm](./images/six/lvm.png)

The logical volume group is divided into logical volumes, which are assigned mount points, such as /home and / m and file system types, such as ext2 or ext3.

* Scan the Logical Volume Manager for available partitions

```
sudo lvmdiskscan //disk scan 
```

* Create Physical Volumes(PV) from the partitions created above

```
sudo pvcreate /dev/xvdf1
sudo pvcreate /dev/xvdg1
sudo pvcreate /dev/xvdh1
```
![physical volume creation](./images/six/pvs.png)


* Verify the creation of those PVs

```
sudo pvs
```

* Add the 3 PVs to a Volume Group called webdata-vg

```
sudo vgcreate webdata-vg /dev/xvdf1 /dev/xvdg1 /dev/xvdh1
```
![volume group creation](./images/six/vgs.png)


* Confirm the creation of the VG

```
sudo vgs
```

* Create two logical volumes - namely, one for app data storage and the other for logs

```
sudo lvcreate -n apps-lv -L 14G webdata-vg
sudo lvcreate -n logs-lv -L 14G webdata-vg
```
![logical volume creation](./images/six/lv.png)


* Verify the creation of the LG 

```
sudo lvs
```


## 8. Confirm the entire setup

```jss
sudo vgdisplay -v // -VG - PV , -LV // volume group display
sudo lsblk
```
![lsblk](./images/six/confirm_setup.png)


## 9. Format the Logical Volumes with ext4 filesystem

```js
sudo mkfs -t ext4 /dev/webdata-vg/apps-lv  // mkfs means make file system
// sudo mkfs -t ext4 /dev/webdata-vg/db-lv  // mkfs means make file system
sudo mkfs -t ext4 /dev/webdata-vg/logs-lv  
```
![formatting logical volumes to ext4 fs type](./images/six/mkfs-ext4.png)


## 10. Create the /var/www/html directory to serve files and /home/recovery/logs for log data backup

```js
sudo mkdir -p /var/www/html 
sudo mkdir -p /home/recovery/logs
```
![mounting to html dir to lv](./images/six/mount_html.png)

## 11. Mount /var/www/html directory on app logical volume 

```js
sudo mount /dev/webdata-vg/apps-lv /var/www/html/
// sudo mount /dev/webdata-vg/db-lv /db/
```

## 12. Backup primary logs in /var/logs/. to recovery dir using rsync then mount the primary log and restore the logs back from the backup.

```js
sudo rsync -av /var/log/. /home/recovery/logs/  // rsync is used to synchronizing files and dir --archive --compress --verbose
sudo mount /dev/webdata-vg/logs-lv /var/log
sudo rsync -av /home/recovery/logs/. /var/log

//https://www.computerhope.com/unix/rsync.htm
```
![mounting to log dir to lv](./images/six/mount_log.png)

## 13. Update the file system table (fstab) at /etc/fstab so config can persist

```js
sudo blkid // block identification. Copy the LV uuid for both apps and logs-lv
sudo vi /etc/fstab // Update the UUID to wht is copied
sudo mount -a //mount entries in our terminal from /etc/fstab file
sudo systemctl daemon-reload
```

![mounting to log dir to lv](./images/six/blkid.png)

![fstab file update](./images/six/fstab.png)


```js
UUID=4c27621b-da64-4010-85b5-47ea5764394e /var/www/html ext4 defaults 0 0
UUID=b82cc037-1bb9-4bd6-a54f-fa96e7acd230 /var/log ext4 defaults 0 0
```

## 14. Verify Updates

```js
df -h
```
![web df](./images/six/web_df.png)


-----------
```js
// sip wine and quick round dance because its about to get crazyyyyy🍷
```


-----
# Repeat No 2 - 14 on the DB Server server
* Instead of apps-lv, create db-lv instead and mount it to /db instead of /var/www/html

-----
## 15.  Run `df -h` to confirm after setting up on db-server

![db df](./images/six/db_df.png)

## 16. Setup mysql on db-server by installing mysql-server and starting it up

```js
sudo yum update -y
sudo yum install mysql-server -y
```

```js
sudo systemctl restart mysqld
sudo systemctl enable mysqld
sudo mysql
```
## 16. Create a user and a database on the db-server

You can get the ip address by running `ip addr show` on the wordpress-server terminal

```js
CREATE DATABASE wordpress;
CREATE USER `client`@`172.31.85.27` IDENTIFIED BY 'mypass';
GRANT ALL ON wordpress.* TO 'client'@'172.31.85.27';
FLUSH PRIVILEGES;
SHOW DATABASES;
exit
```
## 17. Edit `db-server` Inbound rule and open the mysql port, specifying the IP address of the <wordpress-server-private-IP-address:32>
![db df](./images/six/rule.png)



## 18. On the `web-server`, Install wordpress on the WordPress EC2 instance

* Install wget and Apache based dependecies
```js
sudo yum upgrade -y
sudo yum update -y
sudo yum -y install wget httpd php php-mysqlnd php-fpm php-json
// hypertext protocol daemon | MySQL native driver for PHP | PHP FastCGI Process Manager
```

* Start Apache

```js
sudo systemctl enable httpd
sudo systemctl start httpd
```

* Install PHP and its dependencies

```css
sudo yum install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm 
sudo yum install yum-utils http://rpms.remirepo.net/enterprise/remi-release-8.rpm
sudo yum module list php /* Display the current status of a module, including enabled streams and installed profiles */
sudo yum module reset php
sudo yum module enable php:remi-7.4
sudo yum install php php-opcache php-gd php-curl php-mysqlnd
sudo systemctl start php-fpm
sudo systemctl enable php-fpm
sudo setsebool -P httpd_execmem 1
```
* Restart Apache

```js
sudo systemctl restart httpd
```
* WordPress Installation 

```css
mkdir wordpress && cd wordpress
sudo wget http://wordpress.org/latest.tar.gz
sudo tar xzvf latest.tar.gz
sudo rm -rf latest.tar.gz
sudo cp wordpress/wp-config-sample.php wordpress/wp-config.php
sudo cp -R wordpress /var/www/html/
```

* Configure SELinux Policies(Security-Enhanced Linux)

```js
sudo chown -R apache:apache /var/www/html/wordpress
sudo chcon -t httpd_sys_rw_content_t /var/www/html/wordpress -R  //change the SELinux context or TYPE
sudo setsebool -P httpd_can_network_connect=1
```

## 19. Install mysql to test connection to db server
```js
sudo yum install mysql
sudo mysql -u client -p -h 172.31.92.112 // <private-db-server-ip-address> // and input the password
```
## 20. Go to <public-web-ip-address/wordpreses> in this case http://35.171.9.212/wordpress

![db df](./images/six/db_error.png)

We get an error as the db_config has to updated

```js
sudo chmod 777 /var/www/html/wordpress/wp-config.php
sudo vi /var/www/html/wordpress/wp-config.php
```
and update the `db_name`, `db_password`, `db_host` and `db_username`

![db df](./images/six/wp-config.png)

## 21. Save and refresh the file and follow the prompt shown to install and launch wordpress

![db df](./images/six/wordpress_success.png)

