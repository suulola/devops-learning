Ping is a command used to test the reachability of a device on the network using Internet Control Message Protocol (ICMP) protocol.

```js
ping seyi.xyz
```
![Ping](./images/five/ping.png)


Traceroute is a command used to 'trace' the route that a packet takes when traveling to its destination.

```
traceroute seyi.xyz
```
![Traceroute](./images/five/traceroute.png)

### Implementing a Client-Server Architecture with MYSQL
1. Configure 2 EC2 instances in AWS. One as <b>mysql_client</b> and the other as <b>mysql_server</b>
![Create Instance](./images/five/create_instance.png)


2. Install mysql on both mysql_server and client EC2 instances

```
sudo apt update -y
sudo apt upgrade -y
sudo apt install mysql-server
sudo systemctl enable mysql
```
On the mysql_server run the following as well to create a user and the database
```
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'StrongPass@1';
sudo mysql_secure_installation
sudo mysql -p
CREATE USER 'client_remote_user'@'%' IDENTIFIED WITH mysql_native_password BY 'StrongPass@01';
CREATE DATABASE `server_db`;
GRANT ALL ON server_db.* TO 'client_remote_user'@'%';
```
![mysql installations](./images/five/mysql_commands.png)


3. Edit the mysql_server inbound rule to allow the client to communicate with it on port 3306 - specifying the IP address of the client instead of making it totally open
 
![Inbound rules](./images/five/rules.png)

You can see the client IP address right on the terminal

OR 

```js
ip addr show
```

```
ubuntu@ip-172-31-90-203:
```
Here <b>172-31-90-203</b> is the IP address

4. Configure mysql_server to allow client to connect to it remotely - changing the bind-address from 127.0.0.1 t 0.0.0.0
```
sudo vi /etc/mysql/mysql.conf.d/mysqld.cnf
```
![Config](./images/five/mysql_config.png)

5. Restart the service

```
sudo systemctl restart mysql
```
![Restart mysql](./images/five/restart.png)

6. Connect to mysql on the client using

```
sudo mysql -u client_remote_user -h server_public_IP_address -p
```


![Inbound rules](./images/five/client_connection.png)
