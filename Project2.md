# LEMP STACK IMPLEMENTATION

* L- Linux Operating System
* E- Nginx Server
* M- MySQL Database
* P- PHP

1. Create an EC2 instance on AWS
![Project 2 AWS instance](./images/two/aws.png)

2. Update and install nginx web server

```
sudo apt update
sudo apt install nginx
```

![apt update and nginx installation](./images/two/install_nginx.png)
3. Verify the installation

```
sudo systemctl status nginx
curl http://localhost:80
```

The first command checks if the server is running while the second checks if the port is accessible on port 80 and then access it through the public IP address port 80
![verify nginx installation](./images/two/verify_install.png)
![verify nginx loads on browser](./images/two/nginx_browser.png)

4. Database Setup

###### Install mysql using

```
sudo apt install mysql-server
```
![mysql-server installation](./images/two/mysql.png)

###### Launch mysql console using this command as a root user

```
sudo mysql
```
###### Update password of root user to a stronger password 

```
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'StrongPass@1';
exit
```
###### Run script to update security setting - to be more secure and configure the VALIDATE PASSWORD PLUGIN and follow the prompts

```
sudo mysql_secure_installation
```
![mysql security](./images/two/password.png)

###### When done, login back using the commmand to test password update

```
sudo mysql -p
```
######  Remember, you can always quit the db anytime using

```
exit
```

5. PHP Installation

```
sudo apt install php-mysql   // a PHP module that allows PHP to communicate with MySQL-based databases
sudo apt install php-fpm    //PHP fastCGI process manager
```
![PHP installation](./images/two/php_install.png)

###### Core PHP packages will automatically be installed as dependencies.

6. PHP Configuration

#### 🚀 To host more than one domain on a single server, we need to create another server block apart from the default(/var/www/html). 
###### To start, create the root directory for the new domain
```
sudo mkdir /var/www/projectLemp
```

###### Assign directory ownership to currently logged in user
```
sudo chown -R $USER:$USER \var\www\projectLemp
```
 ##### Open a new configuration file in Nginx’s sites-available using vim 
  ```
  sudo nano /etc/nginx/sites-available/projectLemp
 ```
![Set up](./images/two/setup.png)

##### and paste the below

 ```
#/etc/nginx/sites-available/projectLemp

server {
    listen 80;
    server_name projectLEMP www.projectLemp;
    root /var/www/projectLEMP;

    index index.html index.htm index.php;

    location / {
        try_files $uri $uri/ =404;
    }

    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/var/run/php/php8.1-fpm.sock;
     }

    location ~ /\.ht {
        deny all;
    }

}
 ```

 ##### After editing, active configuration by linking file to Nginx’s sites-enabled directory

 ```
sudo ln -s /etc/nginx/sites-available/projectLemp /etc/nginx/sites-enabled/
```

##### Test configuration for syntax error

```
sudo nginx -t
```

##### Disable default Nginx host running on port 80

```
sudo unlink /etc/nginx/sites-enabled/default
```

##### Reload nginx to apply changes and preview

```
sudo systemctl reload ngnix
```

##### To test, create either an index.html or index.php in /var/www/projectLemp to test

```
sudo echo 'Hello LEMP from hostname' $(curl -s http://169.254.169.254/latest/meta-data/public-hostname) 'with public IP' $(curl -s http://169.254.169.254/latest/meta-data/public-ipv4) > /var/www/projectLemp/index.html
```
OR
```
touch /var/www/projectLemp/index.php
sudo echo '<?php phpinfo();' >  /var/www/projectLemp/index.php
```

##### Site can now be accessed using the public IP or server domain
![Page](./images/two/info.png)


7. CRUD operation with PHP and MYSQL


```
sudo mysql -p
```
MySQL based Operations
```
CREATE DATABASE `sample_db`;
CREATE USER 'suu'@'%' IDENTIFIED WITH mysql_native_password BY 'PassWord@123';
GRANT ALL ON sample_db.* TO 'suu'@'%';
exit
```

```
mysql -u suu -p
```

```
SHOW DATABASES;
CREATE TABLE sample_db.todo_list (
  item_id INT AUTO_INCREMENT,
  content VARCHAR(255),
  PRIMARY KEY(item_id)
);
SELECT * FROM sample_db.todo_list;
INSERT INTO sample_db.todo_list (content) VALUES ("My first thing to do");
SELECT * FROM sample_db.todo_list;
exit
``` 

Create a php file that fetches the data and displays it

```
nano /var/www/projectLemp/todo.php
```

Paste 

```
<?php
$user = "suu";
$password = "PassWord@123";
$database = "sample_db";
$table = "todo";

try {
  $db = new PDO("mysql:host=localhost;dbname=$database", $user, $password);
  echo "<h2>TODO</h2><ol>";
  foreach($db->query("SELECT content FROM $table") as $row) {
    echo "<li>" . $row['content'] . "</li>";
  }
  echo "</ol>";
} catch (PDOException $e) {
    print "Error!: " . $e->getMessage() . "<br/>";
    die();
}
```


##### Site can now be accessed using the public IP or server domain /todo.php
![Todo](./images/two/todo.png)
