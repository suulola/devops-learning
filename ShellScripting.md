1. Create the onboard.sh file 


2. Copy file to the server using

```
scp -i suu.pem onboard.sh ubuntu@3.95.223.216:~/;
```
![Copy file from local to remote server](./images/shell/scp.png)

2. SSH into the remote server

```
ssh -i "suu.pem" ubuntu@ec2-3-95-223-216.compute-1.amazonaws.com
```
3. Move script into Shell Folder
```
mkdir Shell
 mv onboard.sh Shell/
```
![Move script to shell directory](./images/shell/move.png)


4. Create SSH specific files and point it such that the id_rsa is created inside the Shell folder

```
cd Shell
ssh-keygen -b 2048 -t rsa
```
![generate rsa key](./images/shell/rsa.png)

5. Create the names.csv file and populate it

```
echo -e "suu\nayomide\ndare\nmane\nsalt" >> names.csv
```
![Create and Populate csv](./images/shell/csv.png)


6. Create the developer group

```
sudo groupadd developers
```

7. Ensure the onboard.sh file is executable

```
sudo chmod +x ./onboard.sh 
```

8. Run the script

```
sudo ./onboard.sh
```

9. To test, copy the private key in the id_rsa file and paste it in a pem file on local

```
cat id_rsa // on remote
vi access.pem // on local and paste the content of the cat then :wqa!
sudo chmod 600 ./access.pem
ssh -i access.pem suu@3.95.223.216
```
![Connecting with user suu](./images/shell/suu.png)
